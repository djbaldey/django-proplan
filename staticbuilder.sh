#!/usr/bin/env bash
#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#

project="proplan"

log_critical() { echo -e "\e[41m${1}\e[0m"; };
log_error() { echo -e "\e[31m${1}\e[0m"; };
log_warn() { echo -e "\e[33m${1}\e[0m"; };
log_info() { echo -e "\e[32m${1}\e[0m"; };

if [ -f "${project}/__init__.py" ]
then
    VERSION=$(python3 -c "import ${project}; print(${project}.__version__);");
else
    log_critical "This script must be run from it directory!";
    exit 1;
fi;

if [ ! -x "$(which lessc)" ]
then
    log_critical "Please setup the 'less':";
    log_critical "sudo npm i less -g";
    exit 1;
fi;

if [ ! -x "$(which cleancss)" ]
then
    log_critical "Please setup the 'cleancss':";
    log_critical "sudo npm i cleancss -g";
    exit 1;
fi;

if [ ! -x "$(which terser)" ]
then
    log_critical "Please setup the 'terser':";
    log_critical "sudo npm i terser -g";
    exit 1;
fi;

PROJECT_DIR="$(pwd)/${project}";

log_warn "-----------------";
log_warn "${project} version: ${VERSION}";
log_warn "lessc version: $(lessc -v | awk '{print($2)}')";
log_warn "clean-css version: $(cleancss --version)";
log_warn "terser version: $(terser --version | awk '{print $2}')";
log_warn "-----------------";


build_less() {
    src="${1}/css/${2}.less"
    mid="${1}/css/${2}.css"
    dst="${1}/css/${2}.min.css"
    lessc ${src} ${mid};
    if [ -s ${mid} ]
    then
        # Compessing the file:
        cleancss -o "${dst}" "${mid}";
        log_info "    OK $(wc -c ${dst})";
        if [ -n "${3}" ]
        then
            # Removing the uncompressed file:
            log_warn " CLEAR $(wc -c ${mid})";
            rm ${mid};
        fi;
    else
        log_error " ERROR $(wc -c ${src})";
        log_error " ERROR $(wc -c ${mid})";
    fi;
}


build_js() {
    src="${1}/js/${2}.js";
    dst="${1}/js/${2}.min.js";
    if [ -s ${src} ]
    then
        # Compessing the file:
        terser -c -m -o "${dst}" "${src}";
        log_info "    OK $(wc -c ${dst})";
        if [ -n "${3}" ]
        then
            # Removing the uncompressed file:
            log_warn " CLEAR $(wc -c ${src})";
            rm ${src};
        fi;
    else
        log_error " ERROR $(wc -c ${src})";
    fi;
}

###############################################################################

echo "STARTS the creation of \"${project}\" files";

STATIC_DIR="${PROJECT_DIR}/static/${project}";

build_less "${STATIC_DIR}" "style" "clear";

# Joining the multiple JS files into one:
# cat ${STATIC_DIR}/js/first.js > ${STATIC_DIR}/js/app.js;
# cat ${STATIC_DIR}/js/second.js >> ${STATIC_DIR}/js/app.js;
# ...

# build_js "${STATIC_DIR}" "app";

echo "   END the creation of \"${project}\" files";

###############################################################################
echo "EXIT";
exit 0;
