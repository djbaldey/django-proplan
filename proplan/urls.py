#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
from django.urls import path, re_path, include
from proplan.views import pages, api, auto

app_name = 'proplan'

abs_patterns = [
    path('', auto.check, name='check'),
    path('create/', auto.CreateView.as_view(), name='create'),
    path('upload/', auto.UploadView.as_view(), name='upload'),
]

api_patterns = [
    path('attachments/', api.AttachmentsView.as_view(), name='attachments'),
    path('attachments/<int:id>/', api.AttachmentView.as_view(), name='attachment'),
    path('comments/', api.CommentsView.as_view(), name='comments'),
    path('comments/<int:id>/', api.CommentView.as_view(), name='comment'),
    path('executors/', api.ExecutorsView.as_view(), name='executors'),
    path('executors/<int:id>/', api.ExecutorView.as_view(), name='executor'),
    path('roles/', api.RolesView.as_view(), name='roles'),
    path('roles/<int:id>/', api.RoleView.as_view(), name='role'),
    path('stages/', api.StagesView.as_view(), name='stages'),
    path('stages/<int:id>/', api.StageView.as_view(), name='stage'),
    path('start/<int:id>/', api.StartView.as_view(), name='start'),
    path('stop/<int:id>/', api.StopView.as_view(), name='stop'),
    path('subprojects/', api.SubprojectsView.as_view(), name='subprojects'),
    path('subprojects/<int:id>/', api.SubprojectView.as_view(), name='subproject'),
    path('tasks/', api.TasksView.as_view(), name='tasks'),
    path('tasks/<int:id>/', api.TaskView.as_view(), name='task'),
    path('trackers/', api.TrackersView.as_view(), name='trackers'),
    path('trackers/<int:id>/', api.TrackerView.as_view(), name='tracker'),
    path('versions/', api.VersionsView.as_view(), name='versions'),
    path('versions/<int:id>/', api.VersionView.as_view(), name='version'),
    path('preview/', api.PreviewView.as_view(), name='preview'),
]


pages_patterns = [
    path('', pages.IndexView.as_view(), name='index'),
    path('attachments/', pages.AttachmentsView.as_view(), name='attachments'),
    path('configuration/', pages.ConfigurationView.as_view(), name='configuration'),
    path('comments/', pages.CommentsView.as_view(), name='comments'),
    path('console/', pages.ConsoleView.as_view(), name='console'),
    path('login/', pages.LoginView.as_view(), name='login'),
    path('logout/', pages.LogoutView.as_view(), name='logout'),
    path('profile/', pages.ProfileView.as_view(), name='profile'),
    path('profile/password/change/', pages.PasswordChangeView.as_view(),
         name='password_change'),
    path('search/', pages.SearchView.as_view(), name='search'),
    path('tasks/', pages.TasksView.as_view(), name='tasks'),
    path('tasks/add/', pages.TaskFormView.as_view(), name='add_task'),
    path('tasks/<int:id>/', pages.TaskView.as_view(), name='task'),
    path('tasks/<int:id>/change/', pages.TaskFormView.as_view(), name='change_task'),
    path('versions/', pages.VersionsView.as_view(), name='versions'),
    re_path(r'^_/(?P<section>[-\w]+)/(?P<name>[-\w]+\.html)$',
            pages.UnderscoreView.as_view(), name='underscore'),
    re_path(r'^_/(?P<name>[-\w]+\.html)$',
            pages.UnderscoreView.as_view(), name='underscore'),
]

urlpatterns = [
    path('', include(pages_patterns)),
    path('abs/', include((abs_patterns, 'abs'))),
    path('api/', include((api_patterns, 'api'))),
]
