/*
 * utils.js
 *
 * Utility functions.
 *
 */

/* eslint no-console: ["error", {allow: ["log"]}] */
/* exported
    checkLocalStorage,
    toLocalStorage,
    fromLocalStorage,
    floatFormat,
    integerFormat,
    closeModal,
    showError
 */


/* Check if localStorage and JSON converter can be executed. */
function checkLocalStorage() {
    if (!window.localStorage || !window.JSON || !window.JSON.stringify) {
        console.error('localStorage or JSON.stringify is undefined');
        return false
    }
    return true
}


/* Function to add settings to localStorage.
 * `fkey` - full key to value. If it defined then the `key` not used.
 */
function toLocalStorage(key, val, fkey) {
    if (!checkLocalStorage()) return;
    var k = fkey || window.SETTINGS_KEY + key,
        v = JSON.stringify(val);
    localStorage.setItem(k, v);
    return [k, v]
}


/* Function to get settings from localStorage.
 * `fkey` - full key to value. If it defined then the `key` not used.
 */
function fromLocalStorage(key, fkey) {
    if (!checkLocalStorage()) return;
    var val = localStorage.getItem(fkey || window.SETTINGS_KEY + key);
    try {
        return JSON.parse(val)
    } catch (e) {
        return val
    }
}


/* The function of displaying numbers with a point. */
function floatFormat(d, digits) {
    if (digits) {
        var pow = Math.pow(10, parseInt(digits));
        d = Math.round(parseFloat(d) * pow) / pow;
    }
    var formatter = new Intl.NumberFormat(window.LANGUAGE_CODE, {
        minimumFractionDigits: digits,
    });
    return formatter.format(parseFloat(d) || 0.0);
}


/* The function of displaying integers. */
function integerFormat(d) {
    var formatter = new Intl.NumberFormat(window.LANGUAGE_CODE);
    return formatter.format(parseInt(d) || 0);
}


/* The function converts all references found in the text,
 * wrapping them with <a> tag.
 */
function linkReplace(text, inwindow) {
    return text.replace(
        /(https?:\/\/[\.\w\-\:]+\/?)+([\S]+)?/g,
        '<a href="$&"' + (inwindow ? '' : ' target="_blank"') + '>$1</a>'
    )
}


/* Returns a list of IDs retrieved from the string. */
function getListIDFromString(s) {
    return $.map(s.split(','), function(i) {return Number(i) || null})
}
