/*
 * templates.js
 *
 * Обработка шаблонов Underscore.
 *
 */

/* global _ */
/* exported loadTemplate */

window.TEMPLATES = {};
window.TEMPLATES_URL = (window.ROOT_URL || '/') + '_/';


/* Функция подгружает необходимый шаблон, рендерит его на основании данных
 * и полученый HTML передаёт callback-функции.
 * Если в качестве данных передать любую ложь, то вызовет callback-функцию
 * с самим объектом шаблона, а не с результатом его рендеринга.
 */
function loadTemplate(name, data, cb) {
    var t = TEMPLATES,
        f = function() {
            if (data) {
                cb(t[name](data))
            }
            else {
                cb(t[name])
            }
        };
    if (t[name]) {
        f()
    } else {
        $.get(TEMPLATES_URL + name + '.html', function(page) {
            t[name] = _.template(page);
            f()
        }).fail(function(jqXHR, textStatus) {
            console.error(
                'Error', jqXHR.status,
                'The template "' + name + '.html" is not loaded.'
            )
        })
    }
}


/* Функция компилирует все встроенные в страницу шаблоны. */
function buildTemplates() {
    $('script').each(function(i, script) {
        if (script.type === 'text/template') {
            var name = script.id.replace('template-', '');
            if (name) {
                TEMPLATES[name] = _.template(script.innerHTML);
            }
        }
    })
}
