#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
from django.template import Library
from django.utils.timezone import now, timedelta


register = Library()


@register.filter
def tracker_color(tracker):
    if not tracker or tracker.is_defect:
        return 'danger'
    if tracker.is_need:
        return 'warning'
    # is_wish
    return 'secondary'


@register.filter
def stage_color(stage):
    if not stage:
        return 'danger'
    if stage.is_new:
        return 'primary'
    if stage.is_error:
        return 'dark'
    if stage.is_process:
        return 'success'
    if stage.is_pause:
        return 'warning'
    # is_finish
    return 'secondary'


@register.filter
def version_color(version):
    if not version or not version.planned_date:
        return 'danger'
    if version.release_date and version.release_date >= now():
        return 'success'
    if version.bugfix:
        return 'warning'
    if not version.minor:
        return 'primary'
    return 'info'


@register.filter
def priority_color(priority):
    if priority >= 5:
        return 'danger'
    if priority == 4:
        return 'warning'
    if priority == 3:
        return 'primary'
    if priority == 2:
        return 'secondary'
    # < 2
    return 'light'


@register.filter
def deadline_color(deadline):
    if deadline:
        if deadline < now() - timedelta(days=1):
            return 'danger'
        elif deadline < now() + timedelta(days=1):
            return 'warning'
    return 'light'


@register.filter
def task_color(task):
    if task.deadline < now() + timedelta(days=1):
        return deadline_color(task.deadline)
    return tracker_color(task.tracker)
