from django import template

from proplan.models import Tracker, Stage

register = template.Library()


@register.simple_tag
def get_tracker_statuses():
    return Tracker.STATUS_CHOICES


@register.simple_tag
def get_stage_statuses():
    return Stage.STATUS_CHOICES


@register.filter
def get_user_executors(task, user):
    return task.executors.filter(user=user)


@register.filter
def executors_any_current(executors):
    for e in executors:
        if e.is_current:
            return True
    return False


@register.filter
def executors_any_exec(executors):
    for e in executors:
        if e.is_current:
            return True
    return False


@register.filter
def executors_time_work(executors):
    return sum([e.time_work for e in executors])
