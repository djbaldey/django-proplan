#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
from os import path

from django.conf import settings
from django.contrib.auth import views as auth_views
from django.core.exceptions import PermissionDenied
from django.db.models import Q, Count
from django.shortcuts import (
    get_object_or_404, render, redirect, resolve_url, Http404,
)
from django.template import TemplateDoesNotExist
from django.urls import reverse_lazy
from django.urls.exceptions import NoReverseMatch
# from django.utils.translation import gettext as _
from django.views.generic import TemplateView, View

from proplan import __version__, forms, conf
from proplan.models import (
    Parameter, Attachment, Stage, Version, Task, Comment, Subproject,
)
from proplan.logic import Controller
from proplan.periods import week, month
from proplan.views.base import AccessMixin


class Context(dict):
    is_ready = False


_default_context = Context()


def get_default_context():
    if not _default_context.is_ready:
        _default_context.update({
            'DEBUG': settings.DEBUG,
            'VERSION': __version__,
            'LOGIN_URL': resolve_url(conf.LOGIN_URL),
            'LOGOUT_URL': resolve_url(conf.LOGOUT_URL),
        })
        _default_context.is_ready = True
    return _default_context.copy()


class IndexView(AccessMixin, View):
    to = 'proplan:console'

    def get(self, request):
        user = request.user
        url = self.to
        if user.is_authenticated:
            url = Parameter.get_value('index', user.id, default=url)
        try:
            return redirect(url)
        except NoReverseMatch:
            return redirect(self.to)


class UnderscoreView(AccessMixin, View):

    dirname = 'proplan/underscore/'

    def get(self, request, name, section=''):
        tname = path.join(section, name)
        try:
            return render(request, self.dirname + tname)
        except TemplateDoesNotExist:
            raise Http404(
                'The template file with the name `%s` was not found in the '
                'template directory `%s`.' % (tname, self.dirname)
            )


class AttachmentsView(AccessMixin, TemplateView):
    """List of uploaded attachments from user."""
    template_name = 'proplan/attachments.html'
    ctrl = Controller(Attachment)

    @property
    def extra_context(self):
        user = self.request.user
        form = forms.AttachmentForm(user)
        page, orders, filters = self.ctrl.get(self.request)
        ctx = get_default_context()
        ctx.update({
            'page': page,
            'orders': orders,
            'filters': filters,
            'form': form,
            'week_0': week(dst=0)[0].isoformat(),
            'week_1': week(dst=-1)[0].isoformat(),
            'week_2': week(dst=-2)[0].isoformat(),
            'month_0': month(dst=0)[0].isoformat(),
            'month_1': month(dst=-1)[0].isoformat(),
            'month_2': month(dst=-2)[0].isoformat(),
        })
        return ctx


class ConfigurationView(AccessMixin, TemplateView):
    template_name = 'proplan/configuration.html'

    @property
    def extra_context(self):
        ctx = get_default_context()
        ctx['parameters'] = Parameter.objects.filter(user_id=None)
        return ctx


class ConsoleView(AccessMixin, TemplateView):
    """Displays the current working place of user."""
    template_name = 'proplan/console.html'
    ctrl = Controller(Task)

    @property
    def extra_context(self):
        ctx = get_default_context()
        ctx['subprojects'] = Subproject.objects.all()
        return ctx

    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)
        qs = Task.objects.select_related()
        qs = qs.exclude(stage__status__in=[
            Stage.STATUS_ERROR, Stage.STATUS_FINISH,
        ])
        qs = qs.order_by(
            'version__release_date',
            'version__planned_date',
            'version__major',
            'version__minor',
            'version__bugfix',
            '-tracker__status',
            '-priority',
        )
        qs, filters = self.ctrl.filtering(qs, request.GET.copy())
        subproject = filters.get('subproject')
        if subproject:
            ctx['subproject'] = get_object_or_404(Subproject, id=subproject)
        ctx['filters'] = filters
        ctx['current_tasks'] = qs.exclude(stage__status=Stage.STATUS_PAUSE)
        ctx['paused_tasks'] = qs.filter(stage__status=Stage.STATUS_PAUSE)
        return self.render_to_response(ctx)


class VersionsView(AccessMixin, TemplateView):
    """History versions for users."""
    template_name = 'proplan/versions.html'
    ctrl = Controller(Version)

    @property
    def extra_context(self):
        page, orders, filters = self.ctrl.get(self.request)
        ctx = get_default_context()
        ctx.update({
            'page': page,
            'orders': orders,
            'filters': filters,
            'week_0': week(dst=0)[0].isoformat(),
            'week_1': week(dst=-1)[0].isoformat(),
            'week_2': week(dst=-2)[0].isoformat(),
            'month_0': month(dst=0)[0].isoformat(),
            'month_1': month(dst=-1)[0].isoformat(),
            'month_2': month(dst=-2)[0].isoformat(),
        })
        return ctx


class CommentsView(AccessMixin, TemplateView):
    """History comments for users."""
    template_name = 'proplan/comments.html'
    ctrl = Controller(Comment.comments)

    @property
    def extra_context(self):
        page, orders, filters = self.ctrl.get(self.request)
        ctx = get_default_context()
        ctx.update({
            'page': page,
            'orders': orders,
            'filters': filters,
            'week_0': week(dst=0)[0].isoformat(),
            'week_1': week(dst=-1)[0].isoformat(),
            'week_2': week(dst=-2)[0].isoformat(),
            'month_0': month(dst=0)[0].isoformat(),
            'month_1': month(dst=-1)[0].isoformat(),
            'month_2': month(dst=-2)[0].isoformat(),
        })
        return ctx


class LoginView(auth_views.LoginView):
    template_name = 'proplan/login.html'

    @property
    def extra_context(self):
        return get_default_context()

    def get_success_url(self):
        url = self.get_redirect_url()
        return url or resolve_url('proplan:index')


class LogoutView(auth_views.LogoutView):
    template_name = 'proplan/logout.html'

    @property
    def extra_context(self):
        return get_default_context()


class PasswordChangeView(auth_views.PasswordChangeView):
    template_name = 'proplan/password_change_form.html'
    success_url = reverse_lazy('proplan:profile')

    @property
    def extra_context(self):
        return get_default_context()


class ProfileView(AccessMixin, TemplateView):
    """View and edit profile for users."""
    template_name = 'proplan/profile.html'

    @property
    def extra_context(self):
        ctx = get_default_context()
        request = self.request
        user = request.user
        ctx['parameters'] = {
            p.name: p for p in Parameter.objects.filter(user=user)}
        return ctx

    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)
        ctx['form'] = forms.ProfileForm(instance=request.user)
        return self.render_to_response(ctx)

    def post(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)
        user = request.user
        form = ctx['form'] = forms.ProfileForm(
            request.POST, files=request.FILES, instance=user)
        if form.is_valid():
            form.save()
            return redirect('proplan:profile')
        return self.render_to_response(ctx)


class SearchView(AccessMixin, TemplateView):
    template_name = 'proplan/search.html'
    task_ctrl = Controller(Task)
    task_ctrl.search_fields = ['title', 'description']
    comment_ctrl = Controller(Comment)
    comment_ctrl.search_fields = ['message']

    @property
    def extra_context(self):
        ctx = get_default_context()
        query = self.request.GET.get('q', '')
        ctx['query'] = query
        if not query:
            return ctx
        # Search by tasks.
        ctrl = self.task_ctrl
        queryset, filters = ctrl.filtering(ctrl.get_queryset(), {'q': query})
        paginator, page = ctrl.pagination(queryset, 25, 1)
        ctx['tasks'] = page.object_list
        if paginator.count > 25:
            ctx['tasks_other'] = paginator.count - 25
        # Search by comments.
        ctrl = self.comment_ctrl
        queryset, filters = ctrl.filtering(ctrl.get_queryset(), {'q': query})
        paginator, page = ctrl.pagination(queryset, 25, 1)
        ctx['comments'] = page.object_list
        if paginator.count > 25:
            ctx['comments_other'] = paginator.count - 25
        return ctx


class TaskController(Controller):

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.annotate(
            num_comments=Count('comment'),
            num_changes=Count('comment', filter=Q(comment__changes__isnull=False))
        )
        return qs


class TasksView(AccessMixin, TemplateView):
    """List tasks for users."""
    template_name = 'proplan/tasks.html'
    ctrl = TaskController(Task)

    @property
    def extra_context(self):
        ctx = get_default_context()
        ctx['subprojects'] = Subproject.objects.all()
        return ctx

    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)
        page, orders, filters = self.ctrl.get(self.request)
        ctx.update({
            'page': page,
            'orders': orders,
            'filters': filters,
            'week_0': week(dst=0)[0].isoformat(),
            'week_1': week(dst=-1)[0].isoformat(),
            'week_2': week(dst=-2)[0].isoformat(),
            'month_0': month(dst=0)[0].isoformat(),
            'month_1': month(dst=-1)[0].isoformat(),
            'month_2': month(dst=-2)[0].isoformat(),
        })
        subproject = filters.get('subproject')
        if subproject:
            ctx['subproject'] = get_object_or_404(Subproject, id=subproject)
        return self.render_to_response(ctx)


class TaskView(AccessMixin, TemplateView):
    """View task for users."""
    template_name = 'proplan/task.html'
    ctrl = Controller(Task)

    @property
    def extra_context(self):
        return get_default_context()

    def get(self, request, id, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)
        user = request.user
        if not user.has_perm('proplan.view_task'):
            raise PermissionDenied
        ctx['markdown_code_style'] = Parameter.get_value(
            'markdown_code_style', user.id)
        ctx['task'] = task = get_object_or_404(Task, id=id)
        new_comment = Comment(user=user, task=task)
        ctx['form'] = forms.CommentCreateForm(user, instance=new_comment)
        ctx['executors'] = task.executors.order_by('start')
        return self.render_to_response(ctx)


class TaskFormView(AccessMixin, TemplateView):
    """Add new or change task for users."""
    template_name = 'proplan/task_form.html'

    @property
    def extra_context(self):
        ctx = get_default_context()
        return ctx

    def check_permission(self, user, id):
        if not id and not user.has_perm('proplan.add_task'):
            raise PermissionDenied
        if id and not user.has_perm('proplan.change_task'):
            raise PermissionDenied

    def get(self, request, id=None, **kwargs):
        user = request.user
        self.check_permission(user, id)
        ctx = self.get_context_data(**kwargs)
        if id:
            task = get_object_or_404(Task, id=id)
            ctx['form'] = forms.TaskChangeForm(user, instance=task)
            ctx['task'] = task
        else:
            ctx['form'] = forms.TaskCreateForm(user)
        return self.render_to_response(ctx)

    def post(self, request, id=None, **kwargs):
        user = request.user
        self.check_permission(user, id)
        ctx = self.get_context_data(**kwargs)
        if id:
            task = get_object_or_404(Task, id=id)
            form = forms.TaskChangeForm(
                user, request.POST, files=request.FILES, instance=task)
            ctx['task'] = task
        else:
            form = forms.TaskCreateForm(
                user, request.POST, files=request.FILES)
        if form.is_valid():
            task = form.save()
            return redirect('proplan:task', id=task.id)
        ctx['form'] = form
        return self.render_to_response(ctx)
