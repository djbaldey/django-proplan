#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
"""
JSON REST API views for Automatic Bug System.
"""
from django.http import JsonResponse
from django.utils.translation import gettext as _
from django.views.decorators.cache import cache_page
from django.views.generic import View

from proplan import forms
from proplan.conf import ABS_KEYS
from proplan.models import Tracker, Stage
from proplan.views.base import parse_params, ABSMixin, BaseUploadView


@cache_page(60 * 5)
def check(request):
    """Checking the ready ABS."""
    ready = bool(
        ABS_KEYS and
        Tracker.objects.get_abs() and
        Stage.objects.get_abs()
    )
    if ready:
        message = _('ABS готова принимать ошибки.')
    else:
        message = _('ABS не готова принимать ошибки.')
    data = {
        'ready': ready,
        'message': message,
    }
    return JsonResponse(data)


class UploadView(ABSMixin, BaseUploadView):
    """Uploads attachments from Automatic Bug System."""
    pass


class CreateView(ABSMixin, View):
    """Posting tasks from Automatic Bug System."""

    def post(self, request):
        form = forms.ABSTaskCreateForm(parse_params(request))
        if form.is_valid():
            task = form.save()
            data = {
                'id': task.id,
                'title': task.title,
            }
            return JsonResponse(data)
        data = {'errors': form.errors.get_json_data(escape_html=True)}
        return JsonResponse(data, status=400)
