#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import gettext_lazy as _

# All next settings must be within the dictionary PROPLAN, when you
# define them in the file settings.py
conf = getattr(settings, 'PROPLAN', {})

# Function allowing access to the application.
ACCESS_FUNCTION = conf.get('ACCESS_FUNCTION', 'proplan.access.view_task')

# ABS - Automatic Bug System.
# While there is no keys, the system does not work.
ABS_KEYS = set()
ABS_KEYS.update(conf.get('ABS_KEYS') or [])
for key in ABS_KEYS:
    if len(key) < 12:
        raise ImproperlyConfigured(
            'Proplan ABS_KEYS is not secure: %s' % key)
# The cookie name for checking the ABS key.
ABS_COOKIE_NAME = conf.get('ABS_COOKIE_NAME', 'proplanabs')

# Path to uploading files.
ATTACH_UPLOAD_PATH = conf.get(
    'ATTACH_UPLOAD_PATH', 'proplan/attaches/%(date)s/%(code)s/%(filename)s')
# The size of the thumbnails for attached images
ATTACH_THUMB_SIZE = conf.get('ATTACH_THUMB_SIZE', (300, 300))
# List of recognized image extensions to be previewed.
ATTACH_THUMB_EXTENSIONS = conf.get('ATTACH_THUMB_EXTENSIONS', [
    '.png', '.jpg', '.jpeg', '.bmp',
])

# List for choice of priorities.
PRIORITIES = conf.get('PRIORITIES', [
    (1, _('низкий приоритет')),
    (2, _('обычный приоритет')),
    (3, _('высокий приоритет')),
    (4, _('срочный приоритет')),
    (5, _('немедленное исполнение')),
])
# Count the days for deadlines by priorities.
DEADLINE_DAYS_PRIORITIES = conf.get('DEADLINE_DAYS_PRIORITIES', {
    1: 15,
    2: 7,
    3: 5,
    4: 3,
    5: 1,
})

# The URLs to login and logout.
LOGIN_URL = conf.get('LOGIN_URL', 'proplan:login')
LOGOUT_URL = conf.get('LOGOUT_URL', 'proplan:logout')
# The list of fields for changing in the user profile.
PROFILE_FIELDS = conf.get('PROFILE_FIELDS', [
    'username', 'email', 'first_name', 'last_name'
])
