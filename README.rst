=======
Proplan
=======

Django application to assign a tasks, defining bugs and planning of your
project.


Installation
------------

.. code-block:: shell

    pip3 install django-proplan


Quick start
-----------

1. Add "proplan" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'proplan',
    ]

2. Include the polls URLconf in your project urls.py like this::

    path('plan/', include('proplan.urls')),

3. Run `python3 manage.py migrate` to create the Proplan models.

4. Run `python3 manage.py createsuperuser` to create the user if you don't
   have one.

5. Start the development server and visit http://127.0.0.1:8000/plan/
   to login.

6. Visit http://127.0.0.1:8000/plan/ to create your plan of work on project.


Automatic Bug System
--------------------

The Proplan allows you to enable automatic publication of errors that have
occurred in the project through ABS - Automatic Bug System. There are 2
ways to do this:

1. Logging errors directly to server.
2. Sending errors through API.


Logging errors
~~~~~~~~~~~~~~

Connect "proplan.log.ABSHandler" to your LOGGING setting like this::

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        ...
        'handlers': {
            ...
            'abs': {
                'class': 'proplan.log.ABSHandler',
                'level': 'ERROR',
            }
        },
        'loggers': {
            ...
            'django': {
                'handlers': ['abs'],
                'level': 'ERROR',
            },
        },
    }


Sending errors
~~~~~~~~~~~~~~

1. Add "ABS_KEYS" to your PROPLAN setting like this::

    PROPLAN = {
        ...
        'ABS_KEYS': ['cookie-supported-key'],
    }

2. Make POST response with data of bug to API::

    curl -H 'Cookie: proplanabs=cookie-supported-key' \
    --data-urlencode 'title=Bug in mobile app&message=More...' \
    http://127.0.0.1:8000/plan/abs/create/


Settings
--------

All next settings must be within the dictionary `PROPLAN`, when you
define them in the file settings.py

ACCESS_FUNCTION
~~~~~~~~~~~~~~~

Function that checks access to resources. You may want to use:

1. `proplan.access.authenticated` - for authenticated users.
2. `proplan.access.staff` - for employers and superusers.
3. `proplan.access.superuser` - for superusers only.
4. `directapps.access.view_users` - for users with view permission for User
   model.
5. `proplan.access.view_task` - for users with view permission for Task
   model.
6. any custom function.

The default is the internal function `proplan.access.view_task`.

ABS_KEYS
~~~~~~~~

The options for Automatic Bug System. While there is no keys, the system does
not work. By default no keys in the set.

ABS_COOKIE_NAME
~~~~~~~~~~~~~~~

The cookie name for checking the ABS key. By default is `proplanabs`.

ATTACH_UPLOAD_PATH
~~~~~~~~~~~~~~~~~~

Path to uploading files. By default is::

    'proplan/attaches/%(date)s/%(code)s/%(filename)s'

ATTACH_THUMB_SIZE
~~~~~~~~~~~~~~~~~

The size of the thumbnails for attached images. By default is ``(300, 300)``.

ATTACH_THUMB_EXTENSIONS
~~~~~~~~~~~~~~~~~~~~~~~

List of recognized image extensions to be previewed. By default is
``['.png', '.jpg', '.jpeg', '.bmp']``.


PRIORITIES
~~~~~~~~~~

List of recognized image extensions to be previewed. By default is::

    [
        (1, _('low')),
        (2, _('normal')),
        (3, _('high')),
        (4, _('urgent')),
        (5, _('immediate')),
    ]


LOGIN_URL
~~~~~~~~~

Endpoint to sign in. By default is ``proplan:login``.


LOGOUT_URL
~~~~~~~~~~

Endpoint to sign out. By default is ``proplan:logout``.
