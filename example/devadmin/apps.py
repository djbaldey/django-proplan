from django.apps import AppConfig


class DevadminConfig(AppConfig):
    name = 'devadmin'
