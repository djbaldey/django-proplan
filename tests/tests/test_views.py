#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the same license as the current project.
#
import json
import os

from django.conf import settings
from django.contrib.auth.models import User, Permission
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase, Client
from django.urls import reverse

from proplan import models, conf


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
ABS_KEY = list(conf.ABS_KEYS)[0]


class ABSClient(Client):

    def login(self, key=ABS_KEY, *args, **kwargs):
        self.logout()
        # Set the cookie to represent the session.
        session_cookie = conf.ABS_COOKIE_NAME
        self.cookies[session_cookie] = key
        cookie_data = {
            'max-age': None,
            'path': '/',
            'domain': settings.SESSION_COOKIE_DOMAIN,
            'secure': settings.SESSION_COOKIE_SECURE or None,
            'expires': None,
        }
        self.cookies[session_cookie].update(cookie_data)

    def force_login(self, *args, **kwargs):
        return self.login(*args, **kwargs)


class ABSTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user('username')
        self.tracker = models.Tracker.objects.create(name='bug', is_abs=True)
        self.stage = models.Stage.objects.create(name='new', is_abs=True)

    def test_check(self):
        url = reverse('proplan:abs:check')
        # Any user may be check the status.
        client = Client()
        response = client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertTrue(data['ready'], response.content)
        self.assertIn('message', data, response.content)

    def test_upload(self):
        url = reverse('proplan:abs:upload')
        filename = os.path.join(BASE_DIR, 'img', 'test.jpg')
        # User connect is disabled.
        client = Client()
        client.force_login(self.user)
        with open(filename, 'rb') as file:
            response = client.post(url, {'file': file})
            self.assertEqual(response.status_code, 403, response.content)
        # ABS connect with not valid key is disabled.
        client = ABSClient()
        client.login('broken-key')
        with open(filename, 'rb') as file:
            response = client.post(url, {'file': file})
            self.assertEqual(response.status_code, 403, response.content)
        # ABS connect with valid key is enabled.
        client.login()
        with open(filename, 'rb') as file:
            response = client.post(url, {'file': file})
            self.assertEqual(response.status_code, 200, response.content)
            data = response.json()
            self.assertIn('id', data)
            self.assertIn('name', data)
            self.assertIn('url', data)
            self.assertIn('thumb', data)
        for obj in models.Attachment.objects.all():
            obj.delete()

    def test_create(self):
        url = reverse('proplan:abs:create')
        # User connect is disabled.
        client = Client()
        client.force_login(self.user)
        response = client.post(url, {'title': 'Title'})
        self.assertEqual(response.status_code, 403, response.content)

        # ABS connect with not valid key is disabled.
        client = ABSClient()
        client.login('broken-key')
        response = client.post(url, {'title': 'Title'})
        self.assertEqual(response.status_code, 403, response.content)

        # ABS connect with valid key is enabled.
        client.login()
        response = client.post(url, {'title': 'Title'})
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['title'], 'Title')


class APITestCase(TestCase):

    def setUp(self):
        self.user = user = User.objects.create_user('username')
        getter = Permission.objects.get_by_natural_key
        perms = [
            # attachment
            getter('add_attachment', 'proplan', 'attachment'),
            getter('delete_attachment', 'proplan', 'attachment'),
            getter('view_attachment', 'proplan', 'attachment'),
            # tracker
            getter('add_tracker', 'proplan', 'tracker'),
            getter('change_tracker', 'proplan', 'tracker'),
            getter('delete_tracker', 'proplan', 'tracker'),
            getter('view_tracker', 'proplan', 'tracker'),
            # stage
            getter('add_stage', 'proplan', 'stage'),
            getter('change_stage', 'proplan', 'stage'),
            getter('delete_stage', 'proplan', 'stage'),
            getter('view_stage', 'proplan', 'stage'),
            # subproject
            getter('add_subproject', 'proplan', 'subproject'),
            getter('change_subproject', 'proplan', 'subproject'),
            getter('delete_subproject', 'proplan', 'subproject'),
            getter('view_subproject', 'proplan', 'subproject'),
            # role
            getter('add_role', 'proplan', 'role'),
            getter('change_role', 'proplan', 'role'),
            getter('delete_role', 'proplan', 'role'),
            getter('view_role', 'proplan', 'role'),
            # version
            getter('add_version', 'proplan', 'version'),
            getter('change_version', 'proplan', 'version'),
            getter('delete_version', 'proplan', 'version'),
            getter('view_version', 'proplan', 'version'),
            # task
            getter('add_task', 'proplan', 'task'),
            getter('change_task', 'proplan', 'task'),
            getter('delete_task', 'proplan', 'task'),
            getter('view_task', 'proplan', 'task'),
            # comment
            getter('add_comment', 'proplan', 'comment'),
            getter('change_comment', 'proplan', 'comment'),
            getter('delete_comment', 'proplan', 'comment'),
            getter('view_comment', 'proplan', 'comment'),
            # executor
            getter('add_executor', 'proplan', 'executor'),
            getter('change_executor', 'proplan', 'executor'),
            getter('delete_executor', 'proplan', 'executor'),
            getter('view_executor', 'proplan', 'executor'),
            getter('start_other_executor', 'proplan', 'executor'),
            getter('stop_other_executor', 'proplan', 'executor'),
        ]
        user.user_permissions.add(*perms)
        self.client = client = Client()
        client.force_login(self.user)

    def test_attachments(self):
        url = reverse('proplan:api:attachments')
        filename = os.path.join(BASE_DIR, 'img', 'test.jpg')
        with open(filename, 'rb') as file:
            response = self.client.post(url, {'file': file})
            self.assertEqual(response.status_code, 200, response.content)
            data = response.json()
            self.assertIn('id', data)
            self.assertIn('name', data)
            self.assertIn('url', data)
            self.assertIn('thumb', data)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertIn('page', data)
        self.assertEqual(data['page']['count'], 1)
        self.assertIn('objects', data['page'])
        self.assertIn('orders', data)
        self.assertIn('filters', data)
        # cleaning attachments from disk.
        for obj in models.Attachment.objects.all():
            obj.delete()

    def test_attachment(self):
        attachment = models.Attachment.objects.create(
            user=self.user,
            file=SimpleUploadedFile('test.txt', b'testing file content')
        )
        url = reverse('proplan:api:attachment', kwargs={'id': attachment.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], attachment.id)
        self.assertIn('created', data)
        self.assertIn('updated', data)
        self.assertIn('file', data)
        self.assertEqual(data['user'], [self.user.id, self.user.username])
        # Removing througth API.
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], attachment.id)
        self.assertEqual(models.Attachment.objects.count(), 0)

    def test_trackers(self):
        url = reverse('proplan:api:trackers')
        # posting
        response = self.client.post(url, {
            'name': 'Name', 'description': 'Description', 'is_abs': True,
        })
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertIn('id', data)
        self.assertEqual(data['name'], 'Name')
        self.assertEqual(data['description'], 'Description')
        self.assertTrue(data['is_abs'])
        # viewing
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertIn('page', data)
        self.assertEqual(data['page']['count'], 1)
        self.assertIn('objects', data['page'])
        self.assertIn('orders', data)
        self.assertIn('filters', data)

    def test_tracker(self):
        tracker = models.Tracker.objects.create(name='Name')
        url = reverse('proplan:api:tracker', kwargs={'id': tracker.id})
        # Getting.
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], tracker.id)
        self.assertEqual(data['name'], 'Name')
        self.assertEqual(data['description'], '')
        self.assertFalse(data['is_abs'])
        # Changing.
        new_data = {'name': 'Bug', 'description': 'For ABS', 'is_abs': True}
        response = self.client.patch(
            url, json.dumps(new_data), content_type='application/json',
        )
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], tracker.id)
        self.assertEqual(data['name'], 'Bug')
        self.assertEqual(data['description'], 'For ABS')
        self.assertTrue(data['is_abs'])
        # Removing.
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], tracker.id)
        self.assertEqual(models.Tracker.objects.count(), 0)

    def test_stages(self):
        url = reverse('proplan:api:stages')
        # posting
        response = self.client.post(url, {
            'name': 'Name', 'description': 'Description', 'is_abs': True,
            'status': models.Stage.STATUS_FINISH,
        })
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertIn('id', data)
        self.assertEqual(data['name'], 'Name')
        self.assertEqual(data['description'], 'Description')
        self.assertEqual(data['status'], models.Stage.STATUS_FINISH)
        self.assertFalse(data['is_abs'])
        # viewing
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertIn('page', data)
        self.assertEqual(data['page']['count'], 1)
        self.assertIn('objects', data['page'])
        self.assertIn('orders', data)
        self.assertIn('filters', data)

    def test_stage(self):
        stage = models.Stage.objects.create(name='Name')
        url = reverse('proplan:api:stage', kwargs={'id': stage.id})
        # Getting.
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], stage.id)
        self.assertEqual(data['name'], 'Name')
        self.assertEqual(data['description'], '')
        self.assertEqual(data['status'], models.Stage.STATUS_NEW)
        self.assertFalse(data['is_abs'])
        # Changing.
        new_data = {
            'name': 'Bug', 'description': 'For ABS', 'is_abs': True,
        }
        response = self.client.patch(
            url, json.dumps(new_data), content_type='application/json',
        )
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], stage.id)
        self.assertEqual(data['name'], 'Bug')
        self.assertEqual(data['description'], 'For ABS')
        self.assertEqual(data['status'], models.Stage.STATUS_NEW)
        self.assertTrue(data['is_abs'])
        # Removing.
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], stage.id)
        self.assertEqual(models.Stage.objects.count(), 0)

    def test_subprojects(self):
        url = reverse('proplan:api:subprojects')
        # posting
        response = self.client.post(url, {
            'name': 'Name', 'description': 'Description',
        })
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertIn('id', data)
        self.assertEqual(data['name'], 'Name')
        self.assertEqual(data['description'], 'Description')
        # viewing
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertIn('page', data)
        self.assertEqual(data['page']['count'], 1)
        self.assertIn('objects', data['page'])
        self.assertIn('orders', data)
        self.assertIn('filters', data)

    def test_subproject(self):
        subproject = models.Subproject.objects.create(name='Name')
        url = reverse('proplan:api:subproject', kwargs={'id': subproject.id})
        # Getting.
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], subproject.id)
        self.assertEqual(data['name'], 'Name')
        self.assertEqual(data['description'], '')
        # Changing.
        new_data = {
            'name': 'MobileApp',
            'description': 'The mobile application.',
        }
        response = self.client.patch(
            url, json.dumps(new_data), content_type='application/json',
        )
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], subproject.id)
        self.assertEqual(data['name'], 'MobileApp')
        self.assertEqual(data['description'], 'The mobile application.')
        # Removing.
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], subproject.id)
        self.assertEqual(models.Subproject.objects.count(), 0)

    def test_roles(self):
        url = reverse('proplan:api:roles')
        # posting
        response = self.client.post(url, {
            'name': 'Name', 'description': 'Description',
        })
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertIn('id', data)
        self.assertEqual(data['name'], 'Name')
        self.assertEqual(data['description'], 'Description')
        self.assertEqual(data['users'], [])
        # viewing
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertIn('page', data)
        self.assertEqual(data['page']['count'], 1)
        self.assertIn('objects', data['page'])
        self.assertIn('orders', data)
        self.assertIn('filters', data)

    def test_role(self):
        role = models.Role.objects.create(name='Name')
        url = reverse('proplan:api:role', kwargs={'id': role.id})
        # Getting.
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], role.id)
        self.assertEqual(data['name'], 'Name')
        self.assertEqual(data['description'], '')
        self.assertEqual(data['users'], [])
        # Changing.
        new_data = {
            'name': 'Tester',
            'description': 'Users tests the application.',
            'users': [self.user.id],
        }
        response = self.client.patch(
            url, json.dumps(new_data), content_type='application/json',
        )
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], role.id)
        self.assertEqual(data['name'], 'Tester')
        self.assertEqual(data['description'], 'Users tests the application.')
        self.assertEqual(data['users'], [[self.user.id, str(self.user)]])
        # Removing.
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], role.id)
        self.assertEqual(models.Role.objects.count(), 0)

    def test_versions(self):
        url = reverse('proplan:api:versions')
        # posting
        response = self.client.post(url, {
            'major': 0, 'minor': 1, 'description': 'Description',
        })
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertIn('id', data)
        self.assertEqual(data['major'], 0)
        self.assertEqual(data['minor'], 1)
        self.assertEqual(data['bugfix'], '')
        self.assertEqual(data['description'], 'Description')
        self.assertEqual(data['master'], None)
        self.assertEqual(data['planned_date'], None)
        self.assertEqual(data['release_date'], None)
        # viewing
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertIn('page', data)
        self.assertEqual(data['page']['count'], 1)
        self.assertIn('objects', data['page'])
        self.assertIn('orders', data)
        self.assertIn('filters', data)

    def test_version(self):
        version = models.Version.objects.create()
        url = reverse('proplan:api:version', kwargs={'id': version.id})
        # Getting.
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], version.id)
        self.assertEqual(data['major'], 0)
        self.assertEqual(data['minor'], 0)
        self.assertEqual(data['bugfix'], '')
        self.assertEqual(data['description'], '')
        self.assertEqual(data['master'], None)
        self.assertEqual(data['planned_date'], None)
        self.assertEqual(data['release_date'], None)
        # Changing.
        new_data = {
            'major': '1',
            'minor': '1',
            'description': 'First functionality.',
            'master': self.user.id,
        }
        response = self.client.patch(
            url, json.dumps(new_data), content_type='application/json',
        )
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], version.id)
        self.assertEqual(data['major'], 1)
        self.assertEqual(data['minor'], 1)
        self.assertEqual(data['description'], 'First functionality.')
        self.assertEqual(data['master'], [self.user.id, str(self.user)])
        # Removing.
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], version.id)
        self.assertEqual(models.Version.objects.count(), 0)

    def test_tasks(self):
        url = reverse('proplan:api:tasks')
        tracker = models.Tracker.objects.create(name='bug')
        stage = models.Stage.objects.create(name='new')
        attachment = models.Attachment.objects.create(
            user=self.user,
            file=SimpleUploadedFile('test.txt', b'testing file content')
        )
        # posting
        response = self.client.post(url, {
            'title': 'Title', 'tracker': tracker.id, 'stage': stage.id,
            'priority': 1, 'attachments': [attachment.id],
        })
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertIn('id', data)
        self.assertEqual(data['title'], 'Title')
        self.assertEqual(data['tracker'], [tracker.id, str(tracker)])
        self.assertEqual(data['stage'], [stage.id, str(stage)])
        self.assertEqual(data['author'], [self.user.id, str(self.user)])
        self.assertEqual(data['priority'], 1)
        self.assertEqual(data['attachments'], [[attachment.id, str(attachment)]])
        # viewing
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertIn('page', data)
        self.assertEqual(data['page']['count'], 1)
        self.assertIn('objects', data['page'])
        self.assertIn('orders', data)
        self.assertIn('filters', data)

    def test_task(self):
        tracker = models.Tracker.objects.create(name='bug')
        stage = models.Stage.objects.create(name='new')
        task = models.Task.objects.create(
            title='Title', tracker=tracker, stage=stage, author=self.user,
        )
        url = reverse('proplan:api:task', kwargs={'id': task.id})
        # Getting.
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], task.id)
        self.assertIn('created', data)
        self.assertIn('updated', data)
        self.assertEqual(data['title'], 'Title')
        self.assertEqual(data['tracker'], [tracker.id, str(tracker)])
        self.assertEqual(data['stage'], [stage.id, str(stage)])
        self.assertEqual(data['author'], [self.user.id, str(self.user)])
        self.assertEqual(data['priority'], 1)
        self.assertEqual(data['attachments'], [])
        # Changing.
        attachment = models.Attachment.objects.create(
            user=self.user,
            file=SimpleUploadedFile('test.txt', b'testing file content')
        )
        new_data = {
            'title': 'New Title', 'tracker': tracker.id, 'stage': stage.id,
            'priority': 2, 'comment': 'Fixed header', 'attachments': [attachment.id],
        }
        response = self.client.patch(
            url, json.dumps(new_data), content_type='application/json',
        )
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['title'], 'New Title')
        self.assertEqual(data['priority'], 2)
        self.assertEqual(data['attachments'], [[attachment.id, str(attachment)]])
        # 1 comment
        self.assertEqual(models.Comment.objects.count(), 1)
        # Removing.
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], task.id)
        self.assertEqual(models.Task.objects.count(), 0)

    def test_comments(self):
        url = reverse('proplan:api:comments')
        tracker = models.Tracker.objects.create(name='bug')
        stage = models.Stage.objects.create(name='new')
        task = models.Task.objects.create(
            title='Title', tracker=tracker, stage=stage, author=self.user,
        )
        # posting
        response = self.client.post(url, {
            'task': task.id, 'message': 'Text of message',
        })
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertIn('id', data)
        self.assertEqual(data['message'], 'Text of message')
        # viewing
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertIn('page', data)
        self.assertEqual(data['page']['count'], 1)
        self.assertIn('objects', data['page'])
        self.assertIn('orders', data)
        self.assertIn('filters', data)

    def test_comment(self):
        tracker = models.Tracker.objects.create(name='bug')
        stage = models.Stage.objects.create(name='new')
        task = models.Task.objects.create(
            title='Title', tracker=tracker, stage=stage, author=self.user,
        )
        comment = models.Comment.objects.create(
            user=self.user, task=task, message='Text',
        )
        url = reverse('proplan:api:comment', kwargs={'id': comment.id})
        # Getting.
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], comment.id)
        self.assertIn('created', data)
        self.assertIn('updated', data)
        self.assertEqual(data['task'], [task.id, str(task)])
        self.assertEqual(data['user'], [self.user.id, str(self.user)])
        self.assertEqual(data['message'], 'Text')
        self.assertEqual(data['changes'], '')
        self.assertEqual(data['attachments'], [])
        # Changing.
        attachment = models.Attachment.objects.create(
            user=self.user,
            file=SimpleUploadedFile('test.txt', b'testing file content')
        )
        new_data = {'message': 'New Text', 'attachments': [attachment.id]}
        response = self.client.patch(
            url, json.dumps(new_data), content_type='application/json',
        )
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['message'], 'New Text')
        self.assertEqual(data['attachments'], [[attachment.id, str(attachment)]])
        # Removing.
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], comment.id)
        self.assertEqual(models.Comment.objects.count(), 0)

    def test_executors(self):
        url = reverse('proplan:api:executors')
        tracker = models.Tracker.objects.create(name='bug')
        stage = models.Stage.objects.create(name='new')
        role = models.Role.objects.create(name='developer')
        task = models.Task.objects.create(
            title='Title', tracker=tracker, stage=stage, author=self.user,
        )
        # posting
        response = self.client.post(url, {
            'user': self.user.id,
            'task': task.id,
            'role': role.id,
        })
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertIn('id', data)
        self.assertIn('created', data)
        self.assertIn('updated', data)
        self.assertEqual(data['user'], [self.user.id, str(self.user)])
        self.assertEqual(data['task'], [task.id, str(task)])
        self.assertEqual(data['role'], [role.id, str(role)])
        self.assertIn('start', data)
        self.assertIn('stop', data)
        self.assertIn('sprint', data)
        self.assertIn('time_sprints', data)
        self.assertIn('time_total', data)
        self.assertIn('time_work', data)
        self.assertIn('is_current', data)
        # viewing
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertIn('page', data)
        self.assertEqual(data['page']['count'], 1)
        self.assertIn('objects', data['page'])
        self.assertIn('orders', data)
        self.assertIn('filters', data)

    def test_executor(self):
        tracker = models.Tracker.objects.create(name='bug')
        stage = models.Stage.objects.create(name='new')
        task = models.Task.objects.create(
            title='Title', tracker=tracker, stage=stage, author=self.user,
        )
        role = models.Role.objects.create(name='developer')
        executor = models.Executor.objects.create(
            user=self.user, task=task, role=role,
        )
        url = reverse('proplan:api:executor', kwargs={'id': executor.id})
        # Getting.
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], executor.id)
        self.assertIn('created', data)
        self.assertIn('updated', data)
        self.assertEqual(data['user'], [self.user.id, str(self.user)])
        self.assertEqual(data['task'], [task.id, str(task)])
        self.assertEqual(data['role'], [role.id, str(role)])
        self.assertIn('start', data)
        self.assertIn('stop', data)
        self.assertIn('sprint', data)
        self.assertIn('time_sprints', data)
        self.assertIn('time_total', data)
        self.assertIn('time_work', data)
        self.assertIn('is_current', data)
        # Changing.
        role2 = models.Role.objects.create(name='designer')
        new_data = {'role': role2.id}
        response = self.client.patch(
            url, json.dumps(new_data), content_type='application/json',
        )
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['role'], [role2.id, str(role2)])
        # Removing.
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], executor.id)
        self.assertEqual(models.Executor.objects.count(), 0)

    def test_executor_start_stop(self):
        tracker = models.Tracker.objects.create(name='bug')
        stage = models.Stage.objects.create(name='new')
        task = models.Task.objects.create(
            title='Title', tracker=tracker, stage=stage, author=self.user,
        )
        role = models.Role.objects.create(name='developer')
        executor = models.Executor.objects.create(
            user=self.user, task=task, role=role,
        )
        # Start.
        url = reverse('proplan:api:start', kwargs={'id': executor.id})
        response = self.client.post(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], executor.id)
        self.assertIsNotNone(data['start'])
        self.assertIsNotNone(data['time_total'])
        self.assertIsNotNone(data['time_work'])
        # Stop.
        url = reverse('proplan:api:stop', kwargs={'id': executor.id})
        response = self.client.post(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data['id'], executor.id)
        self.assertIsNotNone(data['stop'])
        self.assertIsNotNone(data['time_total'])
        self.assertIsNotNone(data['time_work'])
        # Blocked getting.
        url = reverse('proplan:api:start', kwargs={'id': executor.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 405, response.content)
        url = reverse('proplan:api:stop', kwargs={'id': executor.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 405, response.content)

    def test_preview_get(self):
        """From GET request."""
        url = reverse('proplan:api:preview')
        response = self.client.get(url, {
            'source': 'Text',
        })
        self.assertEqual(response.status_code, 200, response.content)
        self.assertEqual(response.content, b'<p>Text</p>')
        # HTML force
        response = self.client.get(url, {
            'source': '# Text', 'result': 'html',
        })
        self.assertEqual(response.status_code, 200, response.content)
        self.assertEqual(response.content, b'<h1>Text</h1>')
        # JSON
        response = self.client.get(url, {
            'source': 'Text', 'result': 'json',
        })
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data, {"result": "<p>Text</p>"})

    def test_preview_post(self):
        """From POST request."""
        url = reverse('proplan:api:preview')
        response = self.client.post(url, {
            'source': 'Text',
        })
        self.assertEqual(response.status_code, 200, response.content)
        self.assertEqual(response.content, b'<p>Text</p>')
        # HTML force
        response = self.client.post(url, {
            'source': '# Text', 'result': 'html',
        })
        self.assertEqual(response.status_code, 200, response.content)
        self.assertEqual(response.content, b'<h1>Text</h1>')
        # JSON
        response = self.client.post(url, {
            'source': 'Text', 'result': 'json',
        })
        self.assertEqual(response.status_code, 200, response.content)
        data = response.json()
        self.assertEqual(data, {"result": "<p>Text</p>"})


class PagesTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user('username')

    def access_test(self, url, redirect_to=None):
        client = Client()
        login = reverse('proplan:login') + '?next=' + url
        # Anonymous has no access.
        response = client.get(url)
        self.assertRedirects(response, login)
        # ABS has no access.
        response = ABSClient().get(url)
        self.assertRedirects(response, login)
        # User has access.
        client.force_login(self.user)
        response = client.get(url)
        if redirect_to:
            self.assertRedirects(response, redirect_to)
        else:
            self.assertEqual(response.status_code, 200, response.content)

    def test_index(self):
        url = reverse('proplan:index')
        self.access_test(url, reverse('proplan:console'))

    def test_attachments(self):
        url = reverse('proplan:attachments')
        self.access_test(url)

    def test_configuration(self):
        url = reverse('proplan:configuration')
        self.access_test(url)

    def test_console(self):
        url = reverse('proplan:console')
        self.access_test(url)

    def test_comments(self):
        url = reverse('proplan:comments')
        self.access_test(url)

    def test_login(self):
        reverse('proplan:login')

    def test_logout(self):
        reverse('proplan:logout')

    def test_profile(self):
        url = reverse('proplan:profile')
        self.access_test(url)

    def test_search(self):
        url = reverse('proplan:search')
        self.access_test(url)

    def test_tasks(self):
        url = reverse('proplan:tasks')
        self.access_test(url)
